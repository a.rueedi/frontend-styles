#!/bin/bash
pushd $(dirname $0) || exit 1
pushd ..
files=$(find . -name "*.scss" -not -path "*node_modules*")

for file in $files ; do
	echo $file
	sed "s/~/node_modules\\//g" -i $file
done
